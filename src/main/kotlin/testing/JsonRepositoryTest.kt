package testing

import model.ParaulogicBoard
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import repositories.JsonRepository

internal class JsonRepositoryTest {

    private val jsonRepository = JsonRepository();

    @Test
    fun addWordTest() {

        val boards = mutableListOf<ParaulogicBoard>(ParaulogicBoard(), ParaulogicBoard(), ParaulogicBoard())

        jsonRepository.saveBoards(12, boards)

    }

    @Test
    fun loadAllDataTest(){

        jsonRepository.loadAllData().forEach {

            println(it.key)
            it.value.forEach {

                println(".   ${it.chars[0]}")

            }

        }

    }
}