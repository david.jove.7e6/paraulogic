package repositories

import kotlinx.serialization.*
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.internal.writeJson
import model.ParaulogicBoard
import java.io.File
import java.time.LocalDate

class JsonRepository {

    fun saveBoards(userID: Long, userBoards: MutableList<ParaulogicBoard>){

        println(File("src/main/resources/$userID").mkdir())

        val userfile = File("src/main/resources/$userID/boards.json")

        userfile.createNewFile()

        userfile.writeText(Json.encodeToString(userBoards))

    }

    fun loadAllData(): MutableMap<Long, MutableList<ParaulogicBoard>> {

        var data = mutableMapOf<Long, MutableList<ParaulogicBoard>>()

        val usersFolders = File("src/main/resources").list { dir, name -> File(dir, name).isDirectory }

        usersFolders.forEach {

            if (File("src/main/resources/$it/boards.json").exists()){

                data.put(it.toLong(), Json.decodeFromString<MutableList<ParaulogicBoard>>(File("src/main/resources/$it/boards.json").readText()))

            }

        }

        return data
    }

}
