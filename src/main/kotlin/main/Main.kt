import com.github.kotlintelegrambot.bot
import com.github.kotlintelegrambot.dispatch
import com.github.kotlintelegrambot.dispatcher.command
import com.github.kotlintelegrambot.dispatcher.text
import com.github.kotlintelegrambot.entities.ChatId
import model.ParaulogicBoard
import repositories.JsonRepository
import java.io.File
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.exists
import kotlin.io.path.readText
import kotlin.io.path.writeText

fun main() {

    var boardExample: ParaulogicBoard? = null
    val repository = JsonRepository()
    var usersBoards = repository.loadAllData()

    val bot = bot {
        token = "5620398972:AAF9_36LUdq2uwFLH73DG2ymEyg1C_tmEtA"
        dispatch {
            command("start") {

                if (usersBoards[message.chat.id] == null){

                    usersBoards[message.chat.id] = mutableListOf<ParaulogicBoard>()

                }

                usersBoards[message.chat.id]!!.add(ParaulogicBoard())

                val result = bot.sendMessage(chatId = ChatId.fromId(message.chat.id),
                    text = "Adivina una paraula formada a partir de les lletres següents: " + usersBoards[message.chat.id]!!.last().chars)

            }
            command("history") {

                var boards = ""

                usersBoards[message.chat.id]?.forEachIndexed { index, it ->

                    if (index == usersBoards.size){

                        boards += "\n\nPARTIDA ACTUAL:"

                    }

                    boards += "\n${it.chars} Paraules endevinades: ${it.guessedWords.size}"

                }

                val result = bot.sendMessage(chatId = ChatId.fromId(message.chat.id),
                    text = "Aquests són els taulells que has jugat: \n" + boards)

            }
            command("guessed") {

                var guessedWords = ""

                usersBoards[message.chat.id]?.last()?.guessedWords?.forEach {

                    guessedWords += "\n$it"

                }

                val result = bot.sendMessage(chatId = ChatId.fromId(message.chat.id),
                    text = "De moment has endevinat aquestes paraules:\n" + guessedWords)

            }
            command("help") {

                val result = bot.sendMessage(chatId = ChatId.fromId(message.chat.id),
                    text = "Per iniciar el joc cal executar la" +
                            " comanda /start. \nPer veure el" +
                            " teu historial de partides (i la teva partida actual), executa " +
                            "la comanda /history." +
                            "\nLa comanda /guessed et recorda les paraules que has endevinat." +
                            " \nSi necessites ajuda, executa la comanda /help.")

            }
            text{
                if (usersBoards[message.chat.id] != null){

                    if (!(text.equals("/start") || text.equals("/history") || text.equals("/help") || text.equals("/guessed"))){

                        if(text in usersBoards[message.chat.id]!!.last().guessedWords){
                            bot.sendMessage(chatId = ChatId.fromId(message.chat.id), text = "Ja hi és!")
                        }
                        else if(usersBoards[message.chat.id]!!.last().guessWord(text)){
                            bot.sendMessage(chatId = ChatId.fromId(message.chat.id), text = "Correcte!")
                        }
                        else if(!usersBoards[message.chat.id]!!.last().guessWord(text)){
                            bot.sendMessage(chatId = ChatId.fromId(message.chat.id), text = "Incorrecte!")
                        }

                    }

                    repository.saveBoards(message.chat.id, usersBoards[message.chat.id]!!);

                }else{

                    bot.sendMessage(chatId = ChatId.fromId(message.chat.id), text =  "Si necessites ajuda, executa la comanda /help.")

                }
            }
        }
    }
    bot.startPolling()

}

//fun telegBot(boardExample : ParaulogicBoard){
//    val bot = bot {
//        token = "5620398972:AAF9_36LUdq2uwFLH73DG2ymEyg1C_tmEtA"
//        dispatch {
//            command("start") {
//                val scanner = Scanner(System.`in`)
//                val result = bot.sendMessage(chatId = ChatId.fromId(message.chat.id),
//                    text = "Adivina una paraula formada a partir de les lletres següents: " + boardExample.chars)
//                boardExample.guessWord(scanner.nextLine())
//            }
//            text{
//                bot.sendMessage(chatId = ChatId.fromId(message.chat.id), text = "Correcte!")
//                if(boardExample.guessWord(text)){
//                    bot.sendMessage(chatId = ChatId.fromId(message.chat.id), text = "Correcte!")
//                }
//
//            }
////            text{
////                if(text.uppercase()!="HOLA")
////                    bot.sendMessage(chatId = ChatId.fromId(message.chat.id), text = "???")
////            }
//            command("today") {
//                val result = bot.sendMessage(chatId = ChatId.fromId(message.chat.id), text = "today: ${Date()}")
//            }
//        }
//    }
//    bot.startPolling()
//}