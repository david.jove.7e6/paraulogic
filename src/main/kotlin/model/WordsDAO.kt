import java.util.*
import kotlin.io.path.Path

class WordsDAO{
    companion object{
        fun fromFileToList() : List<String> {
            val filePath = Path("src/main/resources/words_cat.txt")
            val wordsFile = Scanner(filePath)
            val wordsList = mutableListOf<String>()
            while (wordsFile.hasNext()){
                val word = wordsFile.next()
                wordsList += word
            }
            return wordsList
        }
    }
}

fun wordFinder(wordList : List<String>, charsList : List<Char>) : List<String>{

    val wordsWithChars : MutableList<String> = mutableListOf()

    wordList.forEach { word ->
        var correctWord = true
        for (char in word.uppercase().replace('Ú', 'U').replace('Í', 'I')
            .replace('À', 'A').replace('É', 'E').replace('È', 'E')
            .replace('Ó', 'O').replace('Ò', 'O')) {
            if (char !in charsList) {
                correctWord = false
            }
        }
        if (correctWord && !wordsWithChars.contains(word) && (word.lowercase() == word)) {
            wordsWithChars += word
        }
    }

    return wordsWithChars
}
