package model

import junit.framework.TestCase.assertEquals
import org.junit.Test
import wordFinder

internal class WordsDAOKtTest {


    @Test
    fun wordFinderTest() {
        val wordList = listOf<String>("Hola")
        val charsList = listOf<Char>('H','O','L','A','S','T','G')
        val wordsListCorrect = wordFinder(wordList,charsList)
        assertEquals(listOf("Hola"), wordsListCorrect)
    }
}