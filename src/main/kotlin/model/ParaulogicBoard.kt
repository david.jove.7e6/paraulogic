package model

import WordsDAO
import kotlinx.serialization.Serializable
import wordFinder
import java.time.LocalDate
import kotlin.io.path.Path
import kotlin.math.roundToInt
//import com.google.gson.Gson
import java.io.FileWriter
import java.io.PrintWriter
import java.nio.charset.Charset

@Serializable
class ParaulogicBoard() {

    var chars: MutableList<Char> = mutableListOf<Char>()
    var words = mutableListOf<String>()
    val guessedWords: MutableList<String> = mutableListOf()

    init {
        while (chars.size < 2){

            val vocals = mutableListOf<Int>(65, 69, 73, 79, 85)

            val newVocal = vocals[(Math.random() * (vocals.size-1)).roundToInt()].toChar()

            if (!chars.contains(newVocal)){

                chars.add(newVocal)

            }
        }
        while (chars.size < 7){

            val newChar = ((Math.random() * (90 - 65)) + 65).roundToInt().toChar()

            if (!chars.contains(newChar)){

                chars.add(newChar)

            }
        }
        words = wordFinder(WordsDAO.fromFileToList(),chars) as MutableList<String>
    }
//    constructor(givenChars: MutableList<Char>){
//
//        this.chars = givenChars
//        words = wordFinder(WordsDAO.fromFileToList(),chars) as MutableList<String>
//
//    }


    fun guessWord(userWord: String): Boolean {
        if(words.contains(userWord.lowercase())){
            guessedWords += userWord
        }
        return words.contains(userWord.lowercase())


    }

    // READ AND WRITE JSON
    class Company(var name: String, var employees: Int, var offices: List<String>)

    fun saveTodayWords(){
        val todayDate = "${LocalDate.now()}"
        val filePath = Path("src/main/resources/$todayDate.txt")
        val companies = Company(
            "Microsoft", 182268, listOf("California", "Washington", "Virginia")
        )

        val path = "/json/microsoft.json"
        try {
            PrintWriter(FileWriter(path)).use {
//            val gson = Gson()
//            val jsonString = gson.toJson(companies)
//            it.write(jsonString)
            }
            } catch (e: Exception) {
            e.printStackTrace()
            }
    }
}